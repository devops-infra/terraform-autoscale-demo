# Specify the provider and access details
provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "${var.aws_region}"
}

resource "aws_autoscaling_group" "demo-asg" {
  name = "ops-aws-autoscaling-demo-asg"
  max_size = "${var.scaling.max}"
  min_size = "${var.scaling.min}"
  health_check_grace_period = 600
  health_check_type = "ELB"
  force_delete = true
  launch_configuration = "${aws_launch_configuration.demo-lc.name}"
  load_balancers = ["${aws_elb.demo-elb.name}"]
  vpc_zone_identifier = ["${split(",", var.elb_subnets)}"]
  tag {
        key = "Name"
        value = "ops-aws-autoscaling-demo"
        propagate_at_launch = "true"
    }
  tag {
        key = "ops-demo"
        value = "true"
        propagate_at_launch = "true"
  }
}

resource "aws_launch_configuration" "demo-lc" {
    name = "ops-aws-autoscaling-demo-lc"
    image_id = "${lookup(var.aws_amis, var.aws_region)}"
    instance_type = "${var.instance_type}"
    security_groups = ["${aws_security_group.demo-ec2-sg.id}"]
    user_data = "${file("userdata.sh")}"
    key_name = "${var.key_name}"
}

resource "aws_elb" "demo-elb" {
  name = "ops-aws-autoscaling-demo-elb"
  # The same availability zone as our instances
  subnets = ["${split(",", var.elb_subnets)}"]
  internal = true
  idle_timeout = 60
  connection_draining = true
  connection_draining_timeout = 60
  cross_zone_load_balancing = true
  security_groups = ["${aws_security_group.demo-elb-sg.id}"]
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = 8080
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    target = "HTTP:8080/autoscale/"
    interval = 30
  }
}

resource "aws_security_group" "demo-ec2-sg" {
    name = "ops-aws-autoscaling-demo-ec2-sg"
    description = "Used in the ops aws autoscaling demo for ec2 images"
    vpc_id = "vpc-1428da71"
    # allow ssh from mpac network
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["172.29.0.0/16"]
    }
    # allow http on 8080 to tomcat from mpac network
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["172.29.0.0/16"]
    }
    # full outbound access
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "demo-elb-sg" {
    name = "ops-aws-autoscaling-demo-elb-sg"
    description = "Used in the ops aws autoscaling demo for the elb"
    vpc_id = "vpc-1428da71"
    # allow http on 80 from mpac network
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["172.29.0.0/16"]
    }
    # allow http on 8080 for tomcat and only to the demo tomcat servers
    egress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        security_groups = ["${aws_security_group.demo-ec2-sg.id}"]
    }
}

resource "aws_cloudwatch_metric_alarm" "demo-alarm-grow" {
    alarm_name = "ops-aws-autoscaling-demo-alarm-grow"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "${var.alarm.evaluation_periods}"
    metric_name = "Latency"
    namespace = "AWS/ELB"
    dimensions = {
      LoadBalancerName = "${aws_elb.demo-elb.name}"
    }
    period = "${var.alarm.period}"
    statistic = "Average"
    threshold = "${var.alarm.threshold}"
    alarm_description = "monitor elb response time for autoscaling demo"
    alarm_actions = ["${aws_autoscaling_policy.demo-policy-grow.arn}", "${var.aws_sns_topic}"]
}

resource "aws_cloudwatch_metric_alarm" "demo-alarm-shrink" {
    alarm_name = "ops-aws-autoscaling-demo-alarm-shrink"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "${var.alarm.evaluation_periods}"
    metric_name = "Latency"
    namespace = "AWS/ELB"
    dimensions = {
      LoadBalancerName = "${aws_elb.demo-elb.name}"
    }
    period = "${var.alarm.period}"
    statistic = "Average"
    threshold = "${var.alarm.threshold}"
    alarm_description = "monitor elb response time for autoscaling demo"
    alarm_actions = ["${aws_autoscaling_policy.demo-policy-shrink.arn}", "${var.aws_sns_topic}"]
}


resource "aws_autoscaling_notification" "demo-notification" {
  group_names = [
    "${aws_autoscaling_group.demo-asg.name}"
  ]
  notifications  = [
    "autoscaling:EC2_INSTANCE_LAUNCH", 
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
  ]
  topic_arn = "${var.aws_sns_topic}"
}

resource "aws_autoscaling_policy" "demo-policy-grow" {
  name = "ops-aws-autoscaling-demo-policy-grow"
  scaling_adjustment = "${var.scaling.adjustment}"
  adjustment_type = "ChangeInCapacity"
  cooldown = "${var.scaling.cooldown}"
  autoscaling_group_name = "${aws_autoscaling_group.demo-asg.name}"
}

resource "aws_autoscaling_policy" "demo-policy-shrink" {
  name = "ops-aws-autoscaling-demo-policy-shrink"
  scaling_adjustment = "-${var.scaling.adjustment}"
  adjustment_type = "ChangeInCapacity"
  cooldown = "${var.scaling.cooldown}"
  autoscaling_group_name = "${aws_autoscaling_group.demo-asg.name}"
}

