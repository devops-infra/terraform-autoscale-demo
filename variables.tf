variable "aws_region" {
    description = "The AWS region to create things in."
    default = "us-east-1"
}

# CentOS 7 x86_64 EBS HVM
variable "aws_amis" {
    default = {
        "us-east-1" = "ami-96a818fe"
        "us-west-1" = "ami-6bcfc42e"
        "us-west-2" = "ami-c7d092f7"
    }
}

variable "availability_zones" {
    description = "List of availability zones"
    default = "us-east-1a,us-east-1d"
}

variable "elb_subnets" {
    description = "List of subnets for elb"
    default = "subnet-1e734458,subnet-3e69bd49"
}

variable "key_name" {
    description = "Name of AWS key pair"
    default = "ops-support"
}

variable "instance_type" {
    description = "AWS instance type"
    default = "t2.micro"
}

variable "scaling" {
    default = {

        # minimum servers in scaling group
        "min" = "2"

        # maximum servers in scaling group
        "max" = "6"

        # amount of servers to create or terminate at a time
        "adjustment" = "2"

        # "amount of time to wait after a scale up or scale down event before triggering another"
        "cooldown" = "300"
    }
}

variable "alarm" {
    default = {

        # time period for metric evaluation
        "period" = "60"

        # number of evaluation periods to consider
        "evaluation_periods" = "5"

        # value to compare against (in seconds)
        "threshold" = "5"
    }
}

variable "aws_sns_topic" {
    description = "existing sns topic used to notify during asg events"
    default = "arn:aws:sns:us-east-1:960138600391:ops-topic-brandon"
}

