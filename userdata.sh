#!/bin/bash
sleep 10

yum -y install epel-release
yum -y install ansible git
echo -e "localhost ansible_connection=local" > /etc/ansible/hosts
git clone https://bitbucket.org/devops-infra/ansible-bootstrap.git /tmp/bootstrap
ansible-playbook -v /tmp/bootstrap/init.yml

git clone https://bitbucket.org/devops-infra/ansible-autoscale-demo.git /tmp/autoscale-demo
ansible-playbook -v /tmp/autoscale-demo/init.yml

exit 0