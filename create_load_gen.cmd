for /f "tokens=1" %%F in ('terraform output demo_url')  DO ( set demo_url=%%F )
for /f "tokens=1" %%F in ('terraform output elb_name')  DO ( set elb_name=%%F )

dnscmd sdc1.prd.mpac.ca /recordadd apps autoscale CNAME %elb_name%

echo @echo off > run_load_gen.cmd
echo :top >> run_load_gen.cmd
echo curl %demo_url% >>  run_load_gen.cmd
echo sleep 5 >>  run_load_gen.cmd
echo goto :top >>  run_load_gen.cmd
start "ops  aws autoscaling demo - load generator" run_load_gen.cmd