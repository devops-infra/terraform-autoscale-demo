output "elb_security_group" {
    value = "${aws_security_group.demo-elb-sg.id}"
}
output "ec2_security_group" {
    value = "${aws_security_group.demo-ec2-sg.id}"
}
output "launch_configuration" {
    value = "${aws_launch_configuration.demo-lc.id}"
}
output "asg_name" {
    value = "${aws_autoscaling_group.demo-asg.id}"
}
output "elb_name" {
    value = "${aws_elb.demo-elb.dns_name}"
}
output "demo_url" {
    value = "http://${aws_elb.demo-elb.dns_name}/autoscale/"
}